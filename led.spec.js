var nearley = require("nearley");
var ledGrammar = require("./led.ne");

describe("led", () => {
  var parser;
  var initialState;

  beforeAll(() => {
    parser = new nearley.Parser(
      nearley.Grammar.fromCompiled(ledGrammar, {
        keepHistory: true
      })
    );
    initialState = parser.save();
  });

  afterEach(() => {
    parser.restore(initialState);
  });

  describe("comma delimited", () => {
    [
      {
        value: "LED1, LED2, LED3 3mm or 5mm",
        expected: [
          { id: "LED1", size: "3mm or 5mm" },
          { id: "LED2", size: "3mm or 5mm" },
          { id: "LED3", size: "3mm or 5mm" }
        ]
      }
    ].forEach(testValue => {
      it("does read " + testValue.value, () => {
        parser.feed(testValue.value);
        expect(parser.results[0]).toEqual(testValue.expected);
      });
    });
  });

  describe("single entry", () => {
    it("does read LED 5mm", () => {
      parser.feed("LED 5mm");
      expect(parser.results[0]).toEqual({ id: "LED", size: "5mm" });
    });
  });

  describe("extraneous", () => {
    it("does handle LED 5mm LED, 5mm Adjust LEDR resistor to set brightness depending on the LED used.", () => {
      parser.feed(
        "LED 5mm LED, 5mm Adjust LEDR resistor to set brightness depending on the LED used."
      );
      expect(parser.results[0]).toEqual({
        id: "LED",
        size: "5mm",
        extra:
          "LED, 5mm Adjust LEDR resistor to set brightness depending on the LED used."
      });
    });
  });
});
