var nearley = require("nearley");
var transistorGrammar = require("./transistor.ne");

describe("transitor", () => {
  var parser;
  var initialState;

  beforeAll(() => {
    parser = new nearley.Parser(
      nearley.Grammar.fromCompiled(transistorGrammar, {
        keepHistory: true
      })
    );
    initialState = parser.save();
  });

  afterEach(() => {
    parser.restore(initialState);
  });

  describe("single entry", () => {
    [
      { value: "Q1 J201", expected: { id: "Q1", name: "J201" } },
      { value: "Q15 BD140", expected: { id: "Q15", name: "BD140" } },
      {
        value: "Q15 2N2222A transistor",
        expected: { id: "Q15", name: "2N2222A", extra: "transistor" }
      },
      { value: "Q2 PN2907A", expected: { id: "Q2", name: "PN2907A" } }
    ].forEach(transistorEntry => {
      it("does parse " + transistorEntry.value, () => {
        parser.feed(transistorEntry.value);
        expect(parser.results[0]).toEqual(transistorEntry.expected);
      });
    });
  });
});
