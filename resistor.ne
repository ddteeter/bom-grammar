resistor ->
  resistorIdWithSpace:? resistance extraneous:? {% (d) => {
      var resistanceDetails = {
        resistance: d[1]
      };

      if (d[0] !== null) {
        resistanceDetails.id = d[0];
      }

      if (d[2] !== null) {
        resistanceDetails.extra = d[2];
      }

      return resistanceDetails;
    } %}

resistorIdWithSpace -> resistorId [ ]:+ {% (d) => d[0] %}

resistorId -> (("C" "L" | "L" "E" "D"):? "R") [0-9]:+ {% (d) => {
  return (d[0][0] ? d[0][0].join('') : '') + d[0][1] + d[1].join("");
} %}

resistance -> 
    midUnitResistance {% id %}
  | postUnitResistance {% id %}

midUnitResistance -> [0-9]:+ resistanceUnit [0-9]:+ {% (d) => {
  return {
    quantity: parseFloat(d[0].join('') + (d[2] ? '.' + d[2].join('') : '')),
    unit: d[1]
  };
} %}

postUnitResistance -> [0-9]:+ ("." [0-9]:+):? resistanceUnit:? {% (d) => {
  return {
    quantity: parseFloat(d[0].join('') + (d[1] ? '.' + d[1].join('') : '')),
    unit: (d[2] ? d[2]: 1)
  };
} %}

resistanceUnit ->
    "r"i {% () => 1 %}
  | "k"i {% () => 1000 %}
  | "m"i {% () => 1000000 %}

extraneous -> [ ]:+ .:+ {% (d) => d[1].join('') %}
