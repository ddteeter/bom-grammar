capacitor ->
  capacitorIdWithSpace:? capacitance capacitorTypeWithSpace:? extraneous:? {% (d) => {
      var capacitorDetails = {
        capacitance: d[1]
      };

      if (d[0] !== null) {
        capacitorDetails.id = d[0];
      }

      if (d[2] !== null) {
        capacitorDetails.type = d[2];
      }

      if (d[3] !== null) {
        capacitorDetails.extra = d[3];
      }

      return capacitorDetails;
    } %}

capacitorIdWithSpace -> capacitorId [ ]:+ {% (d) => d[0] %}

capacitorId -> "C" [0-9]:+ {% (d) => {
  return d[0] + d[1].join("");
} %}

capacitance -> [0-9]:+ ("." [0-9]:+):? [ ]:* capacitanceUnit farad:? {% (d) => {
  return {
    quantity: parseFloat(
            d[0].join("") +
            (d[1] ? "."+d[1][1].join("") : "")
        ),
    unit: d[3]
  }
} %}

extraneous -> [ ]:+ .:+ {% (d, _, reject) => {
  var extraneousResult = d[1].join('');

  // Must be kept in sync with capacitorType and unit -- can't figure out how to make this context free
  [
    'film', 'ceramic', 'electro', 'electrolytic', 'mlcc', 
    'tantalum', 'micro', 'u', 'nano', 'n', 'pico', 'p', 'f'
  ].forEach((capacitorType) => {
    if (extraneousResult !== null && extraneousResult !== reject &&
        extraneousResult.toLowerCase().indexOf(capacitorType) === 0) {
      extraneousResult = reject;
    }
  });

  return extraneousResult;
  } 
%}

capacitanceUnit ->
    "micro" {% () => 10 ** -6 %}
  | "u"     {% () => 10 ** -6 %}
  | "nano"  {% () => 10 ** -9 %}
  | "n"     {% () => 10 ** -9 %}
  | "pico"  {% () => 10 ** -12 %}
  | "p"     {% () => 10 ** -12 %}
  | null    {% () => 1 %}

farad -> "F" {% id %} | "f" {% id %}

capacitorTypeWithSpace -> [ ]:+ capacitorType {% (d) => d[1] %}

capacitorType ->
    "film"i {% () => "film" %}
  | "ceramic"i {% () => "ceramic" %}
  | "electro"i {% () => "electrolytic" %}
  | "electrolytic"i {% () => "electrolytic" %}
  | "MLCC"i {% () => "mlcc" %}
  | "tantalum"i {% () => "tantalum" %}
