@builtin "whitespace.ne"
@builtin "number.ne"
@include "./capacitor.ne"
@include "./resistor.ne"
@include "./diode.ne"
@include "./led.ne"
@include "./transistor.ne"
@include "./integrated_circuits.ne"

@{%
    function type(t) {
      return d => [{type: t}].concat(d)
    }
%}

component ->
    capacitor  {% type('capacitor') %}
  | resistor   {% type('resistor') %}
  | diode      {% type('diode') %}
  | led        {% type('led') %}
  | transistor {% type('transistor') %}
  | ic         {% type('integratedCircuit') %}
  | pot        {% type('pot') %}
  | switch     {% type('switch') %}
  | enclosure  {% type('enclosure') %} 
  | jack       {% type('jack') %}
  | header     {% type('header') %}