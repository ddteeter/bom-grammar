var nearley = require("nearley");
var resistorGrammar = require("./resistor.ne");

describe("resistor", () => {
  var parser;
  var initialState;

  beforeAll(() => {
    parser = new nearley.Parser(
      nearley.Grammar.fromCompiled(resistorGrammar, {
        keepHistory: true
      })
    );
    initialState = parser.save();
  });

  afterEach(() => {
    parser.restore(initialState);
  });

  describe("units", () => {
    [
      { value: "10k", expected: { resistance: { quantity: 10, unit: 1000 } } },
      { value: "5k1", expected: { resistance: { quantity: 5.1, unit: 1000 } } },
      { value: "1k5", expected: { resistance: { quantity: 1.5, unit: 1000 } } },
      { value: "2k", expected: { resistance: { quantity: 2, unit: 1000 } } },
      {
        value: "422k",
        expected: { resistance: { quantity: 422, unit: 1000 } }
      },
      { value: "22k", expected: { resistance: { quantity: 22, unit: 1000 } } },
      { value: "2M", expected: { resistance: { quantity: 2, unit: 1000000 } } },
      { value: "2R", expected: { resistance: { quantity: 2, unit: 1 } } },
      { value: "2r", expected: { resistance: { quantity: 2, unit: 1 } } },
      { value: "2K2", expected: { resistance: { quantity: 2.2, unit: 1000 } } },
      { value: "2m", expected: { resistance: { quantity: 2, unit: 1000000 } } },
      { value: "560R", expected: { resistance: { quantity: 560, unit: 1 } } },
      { value: "560", expected: { resistance: { quantity: 560, unit: 1 } } }
    ].forEach(testValue => {
      it("does read " + testValue.value, () => {
        parser.feed(testValue.value);
        expect(parser.results[0]).toEqual(testValue.expected);
      });
    });
  });

  describe("identifiers", () => {
    [
      {
        value: "R1 10k",
        expected: { id: "R1", resistance: { quantity: 10, unit: 1000 } }
      },
      {
        value: "CLR12 10k",
        expected: { id: "CLR12", resistance: { quantity: 10, unit: 1000 } }
      },
      {
        value: "LEDR123 10k",
        expected: { id: "LEDR123", resistance: { quantity: 10, unit: 1000 } }
      }
    ].forEach(testValue => {
      it("does read " + testValue.value, () => {
        parser.feed(testValue.value);
        expect(parser.results[0]).toEqual(testValue.expected);
      });
    });
  });

  describe("extraneous", () => {
    it("does capture extraneous values", () => {
      parser.feed("LEDR1 5k7 Metal film resistor, 1/4W");
      expect(parser.results[0]).toEqual({
        id: "LEDR1",
        resistance: {
          quantity: 5.7,
          unit: 1000
        },
        extra: "Metal film resistor, 1/4W"
      });
    });
  });
});
