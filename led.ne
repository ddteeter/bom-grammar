led -> 
    singleLed {% id %}
  | multiLed {% id %}

singleLed -> ledIdWithSpace:? ledSize extraneous:? {% (d) => {
  var ledDetails = {
    size: d[1]
  };

  if (d[0]) { 
    ledDetails.id = d[0];
  }

  if (d[2]) {
    ledDetails.extra = d[2];
  }

  return ledDetails;
} %}

multiLed -> commaDelimited {% id %}

commaDelimited -> ledId ([ ]:* "," [ ]:* ledId):+ [ ]:+ ledSize extraneous:? {% (d) => {
  var firstEntry = {
    id: d[0],
    size: d[3]
  };

  if (d[4]) {
    firstEntry.extra = d[4];
  }

  return [firstEntry].concat(d[1].map((ledEntry) => {
    var entry = {
      id: ledEntry[3],
      size: d[3]
    };

    if (d[4]) {
      entry.extra = d[4]
    }

    return entry;
  }));
} %}

ledSize ->
    "3mm" {% id %}
  | "5mm" {% id %}
  | "3mm or 5mm" {% id %}

ledIdWithSpace -> ledId [ ]:+ {% (d) => d[0] %}

ledId -> ledPrefix [0-9]:* {% (d) => {
  return d[0] + (d[1] ? d[1].join('') : '');
} %}

ledPrefix -> "LED" {% id %}

extraneous -> [ ]:+ .:+ {% (d, _, reject) => {
    var extraneousResult = d[1].join('');

    ['or', ','].forEach((ledDelimiter) => {
      if (extraneousResult !== null && extraneousResult !== reject &&
        extraneousResult.toLowerCase().indexOf(ledDelimiter) === 0) {
        extraneousResult = reject;
      }
    });

    return extraneousResult;
} %}