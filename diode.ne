diode -> 
    singleDiode {% id %}
  | multiDiode {% id %}

singleDiode -> diodeIdWithSpace:? diodeName extraneous:? {% (d) => {
  var diodeDetails = {
    name: d[1]
  };

  if (d[0]) { 
    diodeDetails.id = d[0];
  }

  if (d[2]) {
    diodeDetails.extra = d[2];
  }

  return diodeDetails;
} %}

multiDiode ->
    semicolonRange {% id %}
  | commaDelimited {% id %}
  | range {% id %}

semicolonRange -> diodeId "-" diodeId (";" [ ]:* diodeId "-" diodeId):+ [ ]:+ diodeName extraneous:? {% (d) => {
    var abbreviation = d[0].substring(0, 1);
    var startId = parseInt(d[0].substring(1));
    var endId = parseInt(d[2].substring(1));

    var diodeEntries = [];
    for (var i = startId; i <= endId; ++i) {
      var entry = {
        id: abbreviation + i,
        name: d[5]
      };

      if (d[6]) {
        entry.extra = d[6];
      }

      diodeEntries.push(entry);
    }

  if (d[3]) {
    for (var i = 0; i < d[3].length; ++i) {
      var rangeEntry = d[3][i];
      var abbreviation = rangeEntry[2].substring(0, 1);
      var startId = parseInt(rangeEntry[2].substring(1));
      var endId = parseInt(rangeEntry[4].substring(1));

      for (var i = startId; i <= endId; ++i) {
        var entry = {
          id: abbreviation + i,
          name: d[5]
        };

        if (d[6]) {
          entry.extra = d[6];
        }

        diodeEntries.push(entry);
      }
    }
  }

  return diodeEntries;
} %}

commaDelimited -> diodeId ([ ]:* "," [ ]:* diodeId):+ [ ]:+ diodeName extraneous:? {% (d) => {
  var firstEntry = {
    id: d[0],
    name: d[3]
  };

  if (d[4]) {
    firstEntry.extra = d[4];
  }

  return [firstEntry].concat(d[1].map((diodeEntry) => {
    var entry = {
      id: diodeEntry[3],
      name: d[3]
    };

    if (d[4]) {
      entry.extra = d[4]
    }

    return entry;
  }));
} %}

range -> diodeId "-" diodeIdWithSpace diodeName extraneous:? {% (d) => {
    var abbreviation = d[0].substring(0, 1);
    var startId = parseInt(d[0].substring(1));
    var endId = parseInt(d[2].substring(1));

    var diodeEntries = [];
    for (var i = startId; i <= endId; ++i) {
      var entry = {
        id: abbreviation + i,
        name: d[3]
      }

      if (d[4]) {
        entry.extra = d[4]
      }

      diodeEntries.push(entry);
    }

    return diodeEntries;
} %}

diodeName ->
    "1" ("N" | "A") [0-9]:+ [abAB]:? {% (d) => d[0] + d[1] + d[2].join('') + (d[3] ? d[3] : '') %}
  | ("BAT" | "BAV" | "BYV") [0-9]:+ {% (d) => d[0] + d[1].join('') %}
  | "MA" [0-9]:+ {% (d) => d[0] + d[1].join('') %}

diodeIdWithSpace -> diodeId [ ]:+ {% (d) => d[0] %}

diodeId -> diodePrefix [0-9]:+ {% (d) => {
  return d[0] + d[1].join("");
} %}

diodePrefix -> "D" | "Z" {% id %}

extraneous -> [ ]:+ .:+ {% (d) => d[1].join('') %}