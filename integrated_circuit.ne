integratedCircuit -> integratedCircuitIdsWithSpace integratedCircuitOptions extraneous:? {% (d, _, reject) => {
  var shouldReject = false;

  var ics = [];
  d[0].forEach(circuitId => {
    var icDetails = {
      id: circuitId,
      name: d[1].name
    };

    if (d[1].alternatives) {
      icDetails.alternatives = d[1].alternatives;
    }

    if (d[2]) {
      icDetails.extra = d[2];
    }

    if ((icDetails.extra && icDetails.extra.indexOf('or ') === 0) ||
        icDetails.name.indexOf(' ') !== -1 ||
        (icDetails.alternatives && !icDetails.alternatives.every(alternative => alternative.indexOf(' ') === -1))) {
      shouldReject = true;
    } else {
      ics.push(icDetails);
    }
  });

  if (shouldReject) {
    return reject;
  } else {
    return ics;
  }
} %}

integratedCircuitOptions -> .:+ ([ ]:+ "or"i [ ]:+ .:+):* {% (d) => {
  var circuits = {
    name: d[0].join('')
  };

  if (d[1].length > 0) {
    circuits.alternatives = d[1].map(alternativeEntry => {
      return alternativeEntry[3].join('');
    });
  }

  return circuits;
} %}

integratedCircuitIdsWithSpace -> integratedCircuitId ("," [ ]:+ integratedCircuitId):* [ ]:+ {% (d) => {
  var circuitIds = [d[0]];

  if (d[1].length > 0) {
    d[1].forEach(circuitIdEntry => {
      circuitIds.push(circuitIdEntry[2]);
    });
  }
  
  return circuitIds;
} %}

integratedCircuitId -> integratedCircuitPrefix [0-9]:+ {% (d) => {
  return d[0] + d[1].join("");
} %}

integratedCircuitPrefix -> "IC" {% id %}

extraneous -> [ ]:+ .:+ {% (d) => d[1].join('') %}