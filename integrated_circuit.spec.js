var nearley = require("nearley");
var icGrammar = require("./integrated_circuit.ne");

describe("integrated circuit", () => {
  var parser;
  var initialState;

  beforeAll(() => {
    parser = new nearley.Parser(
      nearley.Grammar.fromCompiled(icGrammar, {
        keepHistory: true
      })
    );
    initialState = parser.save();
  });

  afterEach(() => {
    parser.restore(initialState);
  });

  describe("multiple entries", () => {
    it("does parse IC1, IC2 4558 DUALTH", () => {
      parser.feed("IC1, IC2 4558 DUALTH");
      expect(parser.results[0]).toEqual([
        { id: "IC1", name: "4558", extra: "DUALTH" },
        { id: "IC2", name: "4558", extra: "DUALTH" }
      ]);
    });
  });

  describe("single entry", () => {
    [
      { value: "IC1 JRC4580D", expected: { id: "IC1", name: "JRC4580D" } },
      {
        value: "IC3 TC1044 or MAX1044 or ICL7660",
        expected: {
          id: "IC3",
          name: "TC1044",
          alternatives: ["MAX1044", "ICL7660"]
        }
      },
      {
        value: "IC1 TL072IP Operational amplifier, DIP8",
        expected: {
          id: "IC1",
          name: "TL072IP",
          extra: "Operational amplifier, DIP8"
        }
      },
      {
        value:
          "IC2 TL072IP Operational amplifier, DIP8 Can also use TL072CP. No sound difference, but TL072IP was cheaper at the time this document was put together.",
        expected: {
          id: "IC2",
          name: "TL072IP",
          extra:
            "Operational amplifier, DIP8 Can also use TL072CP. No sound difference, but TL072IP was cheaper at the time this document was put together."
        }
      }
    ].forEach(icEntry => {
      it("does parse " + icEntry.value, () => {
        parser.feed(icEntry.value);
        expect(parser.results[0]).toEqual([icEntry.expected]);
      });
    });
  });
});
