var nearley = require("nearley");
var diodeGrammar = require("./diode.ne");

describe("diode", () => {
  var parser;
  var initialState;

  beforeAll(() => {
    parser = new nearley.Parser(
      nearley.Grammar.fromCompiled(diodeGrammar, {
        keepHistory: true
      })
    );
    initialState = parser.save();
  });

  afterEach(() => {
    parser.restore(initialState);
  });

  describe("semicolon range", () => {
    [
      {
        value: "D1-D4; D7-D10 MA856",
        expected: [
          { id: "D1", name: "MA856" },
          { id: "D2", name: "MA856" },
          { id: "D3", name: "MA856" },
          { id: "D4", name: "MA856" },
          { id: "D7", name: "MA856" },
          { id: "D8", name: "MA856" },
          { id: "D9", name: "MA856" },
          { id: "D10", name: "MA856" }
        ]
      }
    ].forEach(testValue => {
      it("does read " + testValue.value, () => {
        parser.feed(testValue.value);
        expect(parser.results[0]).toEqual(testValue.expected);
      });
    });
  });

  describe("comma delimited", () => {
    [
      {
        value: "D1,D3,D11,D12 MA856",
        expected: [
          { id: "D1", name: "MA856" },
          { id: "D3", name: "MA856" },
          { id: "D11", name: "MA856" },
          { id: "D12", name: "MA856" }
        ]
      }
    ].forEach(testValue => {
      it("does read " + testValue.value, () => {
        parser.feed(testValue.value);
        expect(parser.results[0]).toEqual(testValue.expected);
      });
    });
  });

  describe("single range", () => {
    [
      {
        value: "D1-D4 MA856",
        expected: [
          { id: "D1", name: "MA856" },
          { id: "D2", name: "MA856" },
          { id: "D3", name: "MA856" },
          { id: "D4", name: "MA856" }
        ]
      }
    ].forEach(testValue => {
      it("does read " + testValue.value, () => {
        parser.feed(testValue.value);
        expect(parser.results[0]).toEqual(testValue.expected);
      });
    });
  });

  describe("single entry", () => {
    it("does read D15 MA856", () => {
      parser.feed("D15 MA856");
      expect(parser.results[0]).toEqual({ id: "D15", name: "MA856" });
    });
  });

  describe("extraneous", () => {
    it(
      "does handle Z1 1N4742A Diode, zener, 12V, DO-41 Overvoltage protection for the TC1044. " +
        "If using a LT1054, you can instead use a 1N4744A (15V zener).",
      () => {
        parser.feed(
          "Z1 1N4742A Diode, zener, 12V, DO-41 Overvoltage protection for the TC1044. " +
            "If using a LT1054, you can instead use a 1N4744A (15V zener)."
        );
        expect(parser.results[0]).toEqual({
          id: "Z1",
          name: "1N4742A",
          extra:
            "Diode, zener, 12V, DO-41 Overvoltage protection for the TC1044. " +
            "If using a LT1054, you can instead use a 1N4744A (15V zener)."
        });
      }
    );
  });

  describe("diode ids", () => {
    [
      {
        value: "D100 1N5817",
        expected: { id: "D100", name: "1N5817" }
      },
      {
        value: "D1 BAT41",
        expected: { id: "D1", name: "BAT41" }
      },
      {
        value: "D3 1N4001",
        expected: { id: "D3", name: "1N4001" }
      },
      {
        value: "Z1 1N4742A",
        expected: { id: "Z1", name: "1N4742A" }
      }
    ];
  });
});
