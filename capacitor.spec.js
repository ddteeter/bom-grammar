var nearley = require("nearley");
var capacitorGrammar = require("./capacitor.ne");

describe("capacitor", () => {
  var parser;
  var initialState;

  beforeAll(() => {
    parser = new nearley.Parser(
      nearley.Grammar.fromCompiled(capacitorGrammar, {
        keepHistory: true
      })
    );
    initialState = parser.save();
  });

  afterEach(() => {
    parser.restore(initialState);
  });

  it("does read full integer capacitance", () => {
    parser.feed("C1 15uF");
    expect(parser.results[0]).toEqual({
      capacitance: { quantity: 15, unit: 0.000001 },
      id: "C1"
    });
  });

  it("does read full decimal capacitance", () => {
    parser.feed("C123 125.34uF");
    expect(parser.results[0]).toEqual({
      capacitance: { quantity: 125.34, unit: 0.000001 },
      id: "C123"
    });
  });

  it("does read integer capacitance only", () => {
    parser.feed("15uF");
    expect(parser.results[0]).toEqual({
      capacitance: { quantity: 15, unit: 0.000001 }
    });
  });

  it("does read decimal capacitance only", () => {
    parser.feed("15.1234uF");
    expect(parser.results[0]).toEqual({
      capacitance: { quantity: 15.1234, unit: 0.000001 }
    });
  });

  it("does read full with capacitor type", () => {
    parser.feed("C123 125.34uF electro");
    expect(parser.results[0]).toEqual({
      capacitance: { quantity: 125.34, unit: 0.000001 },
      id: "C123",
      type: "electrolytic"
    });
  });

  it("does read decimal with capacitor type", () => {
    parser.feed("15.1234uF film");
    expect(parser.results[0]).toEqual({
      capacitance: { quantity: 15.1234, unit: 0.000001 },
      type: "film"
    });
  });

  describe("extraneous data", () => {
    it("does ignore extraneous data at the end on full", () => {
      parser.feed("C1 15.1234uF film capacitor, 7.2 x 2.5mm");
      expect(parser.results[0]).toEqual({
        capacitance: { quantity: 15.1234, unit: 0.000001 },
        id: "C1",
        type: "film",
        extra: "capacitor, 7.2 x 2.5mm"
      });
    });

    it("does ignore extraneous data at the end with type", () => {
      parser.feed("15.1234uF film capacitor, 7.2 x 2.5mm");
      expect(parser.results[0]).toEqual({
        capacitance: { quantity: 15.1234, unit: 0.000001 },
        type: "film",
        extra: "capacitor, 7.2 x 2.5mm"
      });
    });

    it("does ignore extraneous data at the end on value only", () => {
      parser.feed("15.1234uF capacitor, 7.2 x 2.5mm");
      expect(parser.results[0]).toEqual({
        capacitance: { quantity: 15.1234, unit: 0.000001 },
        extra: "capacitor, 7.2 x 2.5mm"
      });
    });

    it("does ignore extraneous data with no type with farad", () => {
      parser.feed("15.1234 F capacitor, 7.2 x 2.5mm");
      expect(parser.results[0]).toEqual({
        capacitance: { quantity: 15.1234, unit: 1 },
        extra: "capacitor, 7.2 x 2.5mm"
      });
    });

    it("does ignore extraneous data with no type or farad", () => {
      parser.feed("15.1234 capacitor, 7.2 x 2.5mm");
      expect(parser.results[0]).toEqual({
        capacitance: { quantity: 15.1234, unit: 1 },
        extra: "capacitor, 7.2 x 2.5mm"
      });
    });
  });

  describe("unit spacing/farad permutations", () => {
    [
      "15.234uF",
      "15.234 micro",
      "15.234 microF",
      "15.234 uf",
      "15.234 u",
      "15.234u",
      "15.234micro",
      "15.234 micro"
    ].forEach(testValue => {
      it("does read " + testValue, () => {
        parser.feed(testValue);
        expect(parser.results[0]).toEqual({
          capacitance: { quantity: 15.234, unit: 0.000001 }
        });
      });
    });
  });

  describe("unit quantity permutations", () => {
    [
      { value: "15.234uF", unit: 0.000001 },
      { value: "15.234microF", unit: 0.000001 },
      { value: "15.234nF", unit: 0.000000001 },
      { value: "15.234nanoF", unit: 0.000000001 },
      { value: "15.234pF", unit: 0.000000000001 },
      { value: "15.234picoF", unit: 0.000000000001 },
      { value: "15.234", unit: 1 },
      { value: "15.234F", unit: 1 }
    ].forEach(testEntry => {
      it("does read " + testEntry.value, () => {
        parser.feed(testEntry.value);
        expect(parser.results[0]).toEqual({
          capacitance: { quantity: 15.234, unit: testEntry.unit }
        });
      });
    });
  });

  describe("capacitor type permutations", () => {
    [
      { value: "15.234uF Electro", type: "electrolytic" },
      { value: "15.234uF electrolytic", type: "electrolytic" },
      { value: "15.234uF film", type: "film" },
      { value: "15.234uF mlcc", type: "mlcc" },
      { value: "15.234uF Ceramic", type: "ceramic" },
      { value: "15.234uF tantalum", type: "tantalum" }
    ].forEach(testEntry => {
      it("does read " + testEntry.value, () => {
        parser.feed(testEntry.value);
        expect(parser.results[0]).toEqual({
          capacitance: { quantity: 15.234, unit: 0.000001 },
          type: testEntry.type
        });
      });
    });
  });
});
