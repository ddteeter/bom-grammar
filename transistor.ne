transitor -> transistorIdWithSpace:? transistorName extraneous:? {% (d) => {
  var transistorDetails = {
    name: d[1]
  };

  if (d[0]) { 
    transistorDetails.id = d[0];
  }

  if (d[2]) {
    transistorDetails.extra = d[2];
  }

  return transistorDetails;
} %}

transistorName ->
    ("2" | "P")  ("N" | "A" | "SA" | "SC" | "SD" | "SK") [0-9]:+ [abAB]:? {% (d) => d[0] + d[1] + d[2].join('') + (d[3] ? d[3] : '') %}
  | (
        "BC" | "BCP" | "BCW" | "BCX" | "BD" | "BDW" | "BF" | "BS" | "BSP" | "BSS" 
      | "BUJ" | "BUL" | "CA" | "FDC" | "FDD" | "FDN" | "FDS" | "FSQP" | "HIT" | "IRF" | "IRFD"
      | "IRFP" | "IRFS" | "IRFZ" | "IRL" | "J" | "KRA" | "KRC" | "KSB" | "KSC" | "KSD" | "KSE"
      | "KSP" | "KSR" | "KST" | "KTA" | "KTB" | "KTC" | "MJE" | "MMBF" | "MPF" | "MPSA" | "ST"
      | "STL" | "STP" | "TIP" | "VN"
    ) [0-9]:+ ("A" | "C" | "D" | "DN" | "FTA" | "-K" | "N" | "P" | "TA"):? {% (d) => d[0] + d[1].join('') + (d[2] ? d[2] : '') %}

transistorIdWithSpace -> transistorId [ ]:+ {% (d) => d[0] %}

transistorId -> transistorPrefix [0-9]:+ {% (d) => {
  return d[0] + d[1].join("");
} %}

transistorPrefix -> "Q" {% id %}

extraneous -> [ ]:+ .:+ {% (d) => d[1].join('') %}